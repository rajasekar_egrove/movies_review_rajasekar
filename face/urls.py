
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from faceapp.views import dashboard, review
import settings


urlpatterns = [
    url(r'^admin/',include(admin.site.urls)),
    url(r'^$',dashboard,name="dashboard"),
    url(r'^review/',review,name="review"),
    url(r'^accounts/', include('registration.backends.default.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

