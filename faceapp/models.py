from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class Profile(AbstractUser):
	address = models.TextField(max_length=500)
	mobile = models.IntegerField(null=True, blank=True)


class ExampleModel(models.Model):
    model_pic = models.ImageField()
    description = models.TextField(null=True, blank=True)
    likes = models.IntegerField(null=True, blank=True)
    dislikes = models.IntegerField(null=True, blank=True)
    rating = models.IntegerField(null=True, blank=True)
    previous_rating = models.IntegerField(null=True, blank=True)


    def __unicode__(self):
    	return "picture %s" %(self.model_pic)

class Comments(models.Model):
    movies =  models.ForeignKey(ExampleModel, on_delete=models.CASCADE)
    comment = models.TextField(max_length=100,null=True, blank=True)
    comment_user =  models.CharField(max_length= 20, blank=True)

    def __unicode__(self):
        return " %s - %s" %(self.comment_user, self.comment)

class Rate(models.Model):
    movies = models.ForeignKey(ExampleModel,on_delete=models.CASCADE)
    rate_user =  models.CharField(max_length= 20, null=True, blank=True)
    rates = models.IntegerField(null=True, blank=True)
    overall_rate = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return "Rating is %s" %(self.rate_user)
