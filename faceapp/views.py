from django.shortcuts import render 
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from faceapp.models import ExampleModel, Comments, Rate
from django.contrib.auth.decorators import login_required
from django.db.models import Avg

import json


@login_required
def dashboard(request):
    if request.method == 'GET':
        picture = ExampleModel.objects.all()
        return render(request, 'faceapp/dashboard.html', {"picture":picture})

@login_required
def review(request):
    
    if request.method == 'POST':
        flag = request.POST.get("flag")
        User=request.user
        movie_id = request.POST.get("id")
        if int(flag)==1:
            likes = request.POST.get("like")
            print likes,"likesssssssssssss"
            likes = int(likes) +1
            movie = ExampleModel.objects.get(id=movie_id)
            movie.likes= likes
            movie.save()
            return HttpResponse(json.dumps({"likes":likes}), content_type="application/json")
        elif int(flag)==0:
            dislikes = request.POST.get("dislike")
            dislikes = int(dislikes) +1
            movie = ExampleModel.objects.get(id=movie_id)
            movie.dislikes = dislikes
            movie.save()
            return HttpResponse(json.dumps({"dislikes":dislikes}), content_type="application/json")
        elif int(flag)==2:
            rating = request.POST.get("a")
            movie = ExampleModel.objects.get(id=movie_id)
            movie.rating = rating
            movie.save()
            return HttpResponse(json.dumps({"rating":rating}), content_type="application/json")
        elif int(flag)==3:
            movie = ExampleModel.objects.get(id=movie_id)
            movie_comments = Comments.objects.filter(movies__id=movie_id)
            text = str(request.POST.get("b"))
            movie.comments_set.create(comment= text, comment_user = User)
            movie.save()
            comment_list = []
            for i in movie_comments:
                user_comments = i.comment
                user = i.comment_user
                comment_list.extend([{'user_comments':user_comments, 'user':user}])
               
            response = {'text' : comment_list,}
            print json.dumps(response)
            return HttpResponse(json.dumps(response), content_type="application/json") 
        else:
            movie = ExampleModel.objects.get(id=movie_id)
            movie_rate = Rate.objects.filter(movies__id=movie_id)
            value = request.POST.get("a")
            movie.rate_set.create(rates= value, rate_user=User)
            overall_rate = Rate.objects.filter(movies__id=movie_id).aggregate(Avg('rates'))
            overall = overall_rate['rates__avg']
            movie.overall_rate = overall
            movie.save()
            rate_list = []
            for j in movie_rate:
                user_rating = j.rates
                user = j.rate_user
                rate_list.extend([{'user_rating':user_rating, 'user':user}])

            response = {'value' :rate_list,'overall' : overall}
            return HttpResponse(json.dumps(response), content_type="application/json")

    else:
        try:
            movie_id = request.GET["id"]
            movie = ExampleModel.objects.get(id=movie_id)
            previous_rate = ExampleModel.objects.filter(id = movie_id).aggregate(Avg('rating'))
            overall_rate = Rate.objects.filter(movies__id=movie_id).aggregate(Avg('rates'))
            previous = previous_rate['rating__avg']
            overall = overall_rate['rates__avg']
            movie_comments = Comments.objects.filter(movies__id=movie_id)
            movie.previous_rating = previous
            movie.overall_rate = overall
            movie.save()
        except ExampleModel.DoesNotExist:
            return HttpResponse("ID is not matched")
        

        return render(request, 'faceapp/review.html', {"movie":movie,"movie_comments":movie_comments,"previous":previous,"overall":overall})      
