# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-03 06:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('faceapp', '0005_examplemodel_comment_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comments',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('movies', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='faceapp.ExampleModel')),
            ],
        ),
    ]
