from django.contrib import admin
from faceapp.models import Profile, ExampleModel, Comments ,Rate

admin.site.register(Profile)
admin.site.register(ExampleModel)
admin.site.register(Comments)
admin.site.register(Rate)

